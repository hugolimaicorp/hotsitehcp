$(document).ready(function(){

	// IMAGENS RANDÔMICAS

    var classCycle=['imageCycle1','imageCycle2','imageCycle3'];

    var randomNumber = Math.floor(Math.random() * classCycle.length);
    var classToAdd = classCycle[randomNumber];
    
    $('#banner').addClass(classToAdd);

    $(".lista li").hover(function(){
    	$(this).find(":after").css({'bacground-color' : 'transparent'});
    });


    //Smooth Scrolling 
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (screen && screen.width < 720) {
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top-109
					}, 1000);
					return false;
				}
			}else{
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top-135
					}, 1000);
					return false;
				}
			}
		}
	});


	//Menu Mobile
	$(".show-menu .c-hamburger").click(function() {
		if ($(".show-menu .c-hamburger").hasClass("is-active")) {
			$(".menu").animate({ "left": "0" }, "fast");
			$(".hide-menu").css({
				'display' : 'block'
			});
		}else{
			$(".menu").animate({ "left": "-100%" }, "fast");
			$(".hide-menu").css({
				'display' : 'none'
			});
		}
	});

	$(".hide-menu").click(function() {
		if ($(".show-menu .c-hamburger").hasClass("is-active")) {
			$(".menu").animate({ "left": "-100%" }, "fast");
			$(".hide-menu").css({
				'display' : 'none'
			});
			$(".show-menu .c-hamburger").removeClass("is-active");
		};
	});
	if (screen && screen.width < 720) {
		$(".menu li a").click(function(){
			setTimeout(function(){
				$(".menu").animate({ "left": "-100%" }, "fast");
				$(".hide-menu").css({
					'display' : 'none'
				});
				$(".show-menu .c-hamburger").removeClass("is-active");
			}, 1100);
		});
	};
});
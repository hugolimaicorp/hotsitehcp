<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title></title>
	<?php /* ?><link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico"><?php */ ?>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>">
	<?php wp_head(); ?>
</head>
<body id="home">
	<div id="container">
		<header id="header">
			<div class="row">
				<div class="mobile-four nine columns">
					<ul class="menu">
						<li><a href="#home" title="Home">Home</a></li>
						<li><a href="#doacao" title="Doação">Doação</a></li>
						<li><a href="#videos" title="Vídeos">Vídeos</a></li>
						<li><a href="http://hcp.org.br" title="Site HCP" target="_blank">Site HCP</a></li>
					</ul>
					<div class="show-menu">
						<button class="c-hamburger c-hamburger--htx">
							<span>toggle menu</span>
						</button>
					</div>
				</div>
				<div class="mobile-four three columns">
					<h1 class="logo"><a href="http://hcp.org.br" target="_blank" title="HCP - Hospital de Câncer de Pernambuco">HCP - Hospital de Câncer de Pernambuco</a></h1>
				</div>
			</div>
		</header>
		<div id="content">
			<section id="banner">
				<div class="row">
					<div class="mobile-four twelve columns">
						<h2 class="logo-abrace"><a href="./" title="Abraçe com coração">Abraçe com coração</a></h2>
					</div>
				</div>
			</section>
			<section id="sobre">
				<div class="row">
					<div class="mobile-four push-one eleven columns laco">
						<h2>Abrace o Hospital de Câncer de Pernambuco.</h2>
						<p>Em 2015, completamos 70 anos dedicados ao tratamento de pacientes com câncer. Em todo este tempo, milhares de pernambucanos foram acolhidos no momento em que mais precisavam. Por isso, estamos aqui pedindo a sua ajuda e o seu abraço, com uma simples doação em dinheiro. Basta fazer uma colaboração mensal. De qualquer valor. Você estará ajudando na manutenção de nossas atividades e no dia a dia de uma das instituições mais antigas do Estado.</p>
					</div>
				</div>
			</section>
			<section id="doacao">
				<div class="row">
					<div class="mobile-four twelve columns">
						<div class="wrap-info clearfix">
							<img class="logo-abrace" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-abrace-grande.png" alt="" class="logo">
							<h4>Estas são histórias reais de pessoas que fazem parte do nosso dia a dia. É por causa delas que a sua ajuda é inestimável. E, seja qual for o valor, toda ajuda é mais do que bem-vinda.</h4>
						</div>
					</div>
					<div class="wrap-personagens clearfix">
						<?php
							wp_reset_query();
							$pessoa = icorp_template_query('pessoa');
						?>
						<?php if ($pessoa->have_posts()):?>
							<?php while($pessoa->have_posts()): $pessoa->the_post(); ?>
								<article class="mobile-four six columns">
									<h2><?php the_title(); ?></h2>
									<?php if ( has_post_thumbnail() ) : ?>
										<?php the_post_thumbnail("pessoa"); ?>
									<?php else: endif; ?>
									<?php the_content(); ?>
									<div class="wrap-doacoes">
										<h4>DOE COM O CORAÇÃO</h4>
										<ul class="valores clearfix">
											<li>
												<form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
													<input type="hidden" name="code" value="<?php echo doacao_codigo_5 ?>" />
													<input type="image" src="wp-content/themes/hotsiteabrace/images/moeda.png" name="submit" alt="Doação de 5 Reais" /><p>5</p>
												</form>
											</li>
											<li>
												<form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
													<input type="hidden" name="code" value="<?php echo doacao_codigo_10 ?>" />
													<input type="image" src="wp-content/themes/hotsiteabrace/images/moeda.png" name="submit" alt="Doação de 10 Reais" /><p>10</p>
												</form>
											</li>
											<li>
												<form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
													<input type="hidden" name="code" value="<?php echo doacao_codigo_30 ?>" />
													<input type="image" src="wp-content/themes/hotsiteabrace/images/moeda.png" name="submit" alt="Doação de 30 Reais" /><p>30</p>
												</form>
											</li>
											<li>
												<form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post" onsubmit="PagSeguroLightbox(this); return false;">
													<input type="hidden" name="code" value="<?php echo doacao_codigo_50 ?>" />
													<input type="image" src="wp-content/themes/hotsiteabrace/images/moeda.png" name="submit" alt="Doação de 50 Reais" /><p>50</p>
												</form>
											</li>
											<li>
												<form action="https://pagseguro.uol.com.br/checkout/v2/donation.html" method="post" target="_blank">
													<input type="hidden" name="currency" value="BRL" />
													<input type="hidden" name="receiverEmail" value="<?php echo doacao_email ?>" />
													<input type="image" src="wp-content/themes/hotsiteabrace/images/moeda.png" name="submit" alt="Doação de outro valor" /><p><span>Outro<br>Valor</span></p>
												</form>
											</li>
										</ul>
									</div>
								</article>
							<?php endwhile; ?>
						<?php else: ?>
							<p>Nenhum cadastrado encontrado.</p>
						<?php endif;?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
			</section>
			<section id="doetambem">
				<div class="row">
					<div class="mobile-four twelve columns">
						<h2>Doe também</h2>
					</div>
				</div>
				<div class="row">
					<div class="wrap-banner-doetambem">
						<a href="javascript:void(0);" taget="_blank">
							<script>
								var mobile = "<img src=\"<?php bloginfo('stylesheet_directory'); ?>/images/banner-300x450.png\" alt=\"\">";
								var desktop = "<img src=\"<?php bloginfo('stylesheet_directory'); ?>/images/banner-940x550.png\" alt=\"\">";
								if (screen && screen.width <= 720) {
									document.write(mobile);
								}else{
									document.write(desktop);
								}
							</script>
						</a>
					</div>
				</div>				
				<?php /* ?>
				<ul class="lista">
					<li class="hvr-shutter-out-vertical"><h4>Tempo</h4></li>
					<li class="hvr-shutter-out-vertical"><h4>Cabelo</h4></li>
					<li class="hvr-shutter-out-vertical"><h4>Alimentos e Suplementos</h4></li>
					<li class="hvr-shutter-out-vertical"><h4>Projetos</h4></li>
				</ul>
				<?php */ ?>
			</section>
			<section id="videos">
				<div class="row">
					<div class="mobile-four twelve columns">
						<h2>Vídeos</h2>
						<div class="video-container">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/MNJRH3CtW88" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</section>
		</div>
		<footer>
			<div class="row">
				<div class="mobile-four six columns">
					<address>Avenida Cruz Cabugá, 1597, 50040-000, Santo Amaro - Recife - PE<br>Ligue: (81) 3217-8000<br>hcp@hcp.org.br</address>
				</div>
				<div class="mobile-four push-one four columns">
					<ul class="social">
						<li><a href="#" target="_blank" title="Facebook">Facebook</a></li>
						<li><a href="#" target="_blank" title="Youtube">Youtube</a></li>
						<li><a href="#" target="_blank" title="Twitter">Twitter</a></li>
						<li><a href="#" target="_blank" title="Instagram">Instagram</a></li>
					</ul>
				</div>
			</div>
		</footer>
	</div>
	<?php wp_footer(); ?>
	<script>
	  (function() {

	    "use strict";

	    var toggles = document.querySelectorAll(".c-hamburger");

	    for (var i = toggles.length - 1; i >= 0; i--) {
	      var toggle = toggles[i];
	      toggleHandler(toggle);
	    };

	    function toggleHandler(toggle) {
	      toggle.addEventListener( "click", function(e) {
	        e.preventDefault();
	        (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
	      });
	    }

	  })();
	</script>
	<a href="javascript:void(0);" class="hide-menu">Esconder Menu</a>
	<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
	<?php if(isset($_GET['retorno'])){ ?>
		<script>
			window.open('http://www.facebook.com/sharer.php?u=<?php echo url_share_facebook ?>','', 'toolbar=0, status=0, width=650, height=450');
		</script>
	<?php } ?>
</body>
</html>
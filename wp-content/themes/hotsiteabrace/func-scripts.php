<?php

add_action('wp_enqueue_scripts', 'icorp_add_scripts');
function icorp_add_scripts() {

    if( !is_admin()){
        wp_deregister_script('jquery');
        wp_register_script('icorp_jquery', (get_bloginfo('stylesheet_directory') . '/scripts/jquery.min.js'), false, '1.11.1');
        wp_enqueue_script('icorp_jquery');
    }

    wp_enqueue_script(
        'icorp_modernizr',
        get_bloginfo('stylesheet_directory') . '/scripts/modernizr.min.js',
        array('icorp_jquery'), time(), false
    );

    wp_enqueue_script(
        'icorp_scripts',
        get_bloginfo('stylesheet_directory') . '/scripts/script.js',
        array('icorp_modernizr'), time(), false
    );
}
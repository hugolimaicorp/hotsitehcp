<?php

function icorp_template_query($template) {

	global $post;
	global $wp_query;

	$template_query['pessoa'] = array(
		'cat' => 2,
		'posts_per_page' => -1
	);

	// início

	$page = array(
	 	'paged' => get_query_var('paged')
	);

	$args = array_merge($template_query[$template], $page);

	// pretty_dump($args);

	$query = new wp_query($args);

	return $query;
}